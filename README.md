===================================================
"linode" Command Line interface to digitalocean.com
===================================================

"linode" is command line tool written in Click.


Installation: 
-------------

git clone https://bitbucket.org/ypanchal/linode

cd linode

python setup.py install


Usage: 
------
# To configure linode
linode --configure

linode --help

extra command help

linode command --help

ex: linode create --help
 
