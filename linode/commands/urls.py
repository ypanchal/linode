# -*- coding: utf-8 -*-

# Linode api urls

LINODE_CREATE = '&api_action=linode.create'
LINODE_BOOT = '&api_action=linode.boot'
LINODE_LIST = '&api_action=linode.list'
LINODE_DISK_CREATE = '&api_action=linode.disk.create'
LINODE_DISK_FROM_IMAGE = '&api_action=linode.disk.createfromimage'
LINODE_IP = '&api_action=linode.ip.list'
AVAIL_DATACENTER = '&api_action=avail.datacenters'
AVAIL_DISTRIBUTION = '&api_action=avail.distributions'
AVAIL_PLANS = '&api_action=avail.linodeplans'
JOB_LIST = '&api_action=linode.job.list'
LINODE_CREATE_CONFIG = '&api_action=linode.config.create'
AVAIL_IMAGE = '&api_action=image.list'
DELETE_LINODE = '&api_action=linode.delete'
